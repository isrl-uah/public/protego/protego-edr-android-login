package com.uah.protegologin.model

import com.google.gson.annotations.SerializedName

data class AccessToken(
    @SerializedName("jwtToken")
    var jwtToken : String
)

