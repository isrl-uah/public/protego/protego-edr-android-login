package com.uah.protegologin

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.uah.protegologin.model.AccessToken
import com.uah.protegologin.repository.Repository
import kotlinx.coroutines.launch
import retrofit2.Response

class MainViewModel(private val repository: Repository): ViewModel() {

    val myResponse: MutableLiveData<Response<AccessToken>> = MutableLiveData()

    fun getAccessToken(
        username: String,
        password: String,
    ) {
        viewModelScope.launch {
            val response = repository.getAccessToken(username=username,password=password)
            myResponse.value = response
        }
    }
}