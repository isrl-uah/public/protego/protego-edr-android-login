package com.uah.protegologin.api

import com.uah.protegologin.JWToken
import com.uah.protegologin.model.AccessToken
import retrofit2.Response
import retrofit2.http.*

interface EDROAuth {

    class Auth(username: String, password: String) {
        private var username: String = username
        private var password: String = password
    }

    @Headers("Content-Type: application/json")
    @POST("login")
    suspend fun getAccessToken(
        @Body auth: Auth
    ): Response<AccessToken>
}
