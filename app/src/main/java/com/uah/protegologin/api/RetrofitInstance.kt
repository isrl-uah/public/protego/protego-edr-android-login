package com.uah.protegologin.api

import com.uah.protegologin.APIConstants.EDRAPI
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object RetrofitInstance {
    // lazy read-only property
    private val retrofit by lazy {

        val interceptor : HttpLoggingInterceptor = HttpLoggingInterceptor().apply {
            this.level = HttpLoggingInterceptor.Level.BODY
        }

        val client : OkHttpClient = OkHttpClient.Builder().apply {
            this.addInterceptor(interceptor)
        }.build()

        Retrofit.Builder()
            .baseUrl(EDRAPI)
            .addConverterFactory(GsonConverterFactory.create())
            .client(client)
            .build()
    }


    val api: EDROAuth by lazy {
        retrofit.create(EDROAuth::class.java)
    }

}