package com.uah.protegologin.repository

import com.uah.protegologin.JWToken
import com.uah.protegologin.api.EDROAuth
import com.uah.protegologin.api.RetrofitInstance
import com.uah.protegologin.model.AccessToken
import retrofit2.Response

class Repository {

    suspend fun getAccessToken(
        username: String,
        password: String,
    ): Response<AccessToken> {
        val auth : EDROAuth.Auth = EDROAuth.Auth(username, password)
        return RetrofitInstance.api.getAccessToken(auth)
    }

}
