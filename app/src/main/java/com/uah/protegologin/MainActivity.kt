package com.uah.protegologin

import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.uah.protegologin.databinding.ActivityMainBinding
import com.uah.protegologin.repository.Repository

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        binding = ActivityMainBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        binding.button.setOnClickListener{
            /*
            val webIntent: Intent = Uri.parse("https://www.android.com").let { webpage ->
                Intent(Intent.ACTION_VIEW, webpage)
            }
             */

            //PROTEGO_EDR_API="https://edr.protego.junquera.xyz"


                Log.d("Login CA", "Calling to ProTego CA Service");

                val repository = Repository()
                val viewModelFactory = MainViewModelFactory(repository)
                val viewModel = ViewModelProvider(this, viewModelFactory).get(MainViewModel::class.java)

                val username =  binding.editTextUsername.text.toString()
                val password = binding.editTextTextPassword.text.toString()
                viewModel.getAccessToken(username = username, password = password)

                viewModel.myResponse.observe(this, Observer { response ->
                    if (response.isSuccessful) {
                        var intent: Intent? = null
                        Log.d("Response", response.body().toString())
                        try {
                            intent = Intent().setClassName("eu.protego.ca.edr.android",
                                    "eu.protego.ca.edr.android.core.receiver.ProTegoJWTReceiver")
                            intent.action = "android.intent.action.SEND"
                        } catch (ignored: Exception) {
                            Log.d("Intent not found", "Error intent")
                        }

                        if (intent != null) {
                            val token = response.body()?.jwtToken
                            val jwt: JWToken = JWToken(username, token)
                            intent.putExtra("jwt", jwt.serialize())
                            sendBroadcast(intent)
                        } else {
                            Log.d("Login CA", "Fail Call to ProTego CA Service");
                        }
                    } else {
                        Log.d("Response", response.errorBody().toString())
                    }
                })



            //val ss:String = intent.getStringExtra("username").toString()

        }
    }
}