package com.uah.protegologin

import android.util.Base64

class JWToken {
    private var username: String? = null
    private var jwtToken: String? = null

    constructor() {}
    constructor(username: String?, jwtToken: String?) {
        this.username = username
        this.jwtToken = jwtToken
    }

    fun serialize(): String {
        return Base64.encodeToString("$username:::$jwtToken".toByteArray(), Base64.DEFAULT)
    }

    companion object {
        fun deserialize(token: String?): JWToken {
            val decoded = String(Base64.decode(token, Base64.DEFAULT))
            val jwToken = JWToken()
            jwToken.username = (decoded.split(":::").toTypedArray()[0])
            jwToken.jwtToken = (decoded.split(":::").toTypedArray()[1])
            return jwToken
        }
    }
}